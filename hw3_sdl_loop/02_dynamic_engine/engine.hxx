#pragma once

#include <iosfwd>
#include <string>
#include <string_view>

#ifndef RH_DECLSPEC
#define RH_DECLSPEC
#endif

namespace rh {

enum class InputSources {
    input_source_none,
    input_source_keypad,
    input_source_controller,

    input_source_count
};

enum class KeyAction {
    key_action_none,
    key_action_pressed,
    key_action_released,

    key_action_count
};

enum class KeyCode {
    key_code_none,
    key_code_up,
    key_code_left,
    key_code_down,
    key_code_right,
    key_code_esc,
    key_code_enter,
    key_code_turn_off,

    key_code_count
};

class RH_DECLSPEC Event {
public:
    Event(const InputSources& src = InputSources::input_source_none, 
          const KeyAction& act = KeyAction::key_action_none, 
          const KeyCode& code = KeyCode::key_code_none, 
          const int joy_num = -1)
        : source(src)
        , key_action(act)
        , key_code(code) 
        , joystick_number(joy_num) {
    };
    bool WriteEventName(std::ostream& stream) const;
    bool CanContinue() const;
	virtual ~Event();

private:
    InputSources source;
    KeyAction    key_action;
    KeyCode      key_code;
    int          joystick_number;
};


RH_DECLSPEC std::ostream& operator<<(std::ostream& stream, const Event& event); 

class Engine;

/// return not null on success
RH_DECLSPEC Engine* create_engine();
RH_DECLSPEC void destroy_engine(Engine* e); 

class RH_DECLSPEC Engine {
public:
    virtual ~Engine();
    virtual std::string Initialize(std::string_view config) = 0; // return errors line
    virtual bool ReadInput(Event& event) = 0;
    virtual void Uninitialize()     = 0;
};

} // end namespace rh
