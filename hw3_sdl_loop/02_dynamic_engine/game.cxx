#include <algorithm>
#include <array>
#include <cstdlib>
#include <iostream>
#include <memory>
#include <string_view>

#include "engine.hxx"

int main(int /*argc*/, char* /*argv*/ []) {
    std::unique_ptr<rh::Engine, void (*)(rh::Engine*)> engine(rh::create_engine(), rh::destroy_engine);

    std::string err = engine->Initialize("");
    if (!err.empty()) {
    	std::cerr << err << std::endl;
    	return EXIT_FAILURE;
    }

    bool continue_loop = true;
    while (continue_loop) {
        rh::Event event;

        while (engine->ReadInput(event)) {
            std::cout << event << std::endl;
            if (!event.CanContinue()) {
                continue_loop = false;
            }
        }
    }

    engine->Uninitialize();

    return EXIT_SUCCESS;
}
