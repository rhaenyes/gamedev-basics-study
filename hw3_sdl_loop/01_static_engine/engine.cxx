#include "engine.hxx"

#include <algorithm>
#include <array>
#include <exception>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <vector>


#if __has_include(<SDL.h>)
#include <SDL.h>
#else
#include <SDL2/SDL.h>
#endif

namespace rh {

static bool engine_already_exist = false;


std::ostream& operator<<(std::ostream& out, const SDL_version& v) {
    out << static_cast<int>(v.major) << '.';
    out << static_cast<int>(v.minor) << '.';
    out << static_cast<int>(v.patch);
    return out;
}

static std::array<std::string_view, static_cast<std::uint32_t>(InputSources::input_source_count)> input_sources_names = {
    "none",
    "keypad",
    "controller"
};

static std::array<std::string_view, static_cast<std::uint32_t>(KeyAction::key_action_count)>  key_action_names = {
    "none",
    "pressed",
    "released" 
};

static std::array<std::string_view, static_cast<std::uint32_t>(KeyCode::key_code_count)> key_code_names = {
    "none",
    "up",
    "left",
    "down",
    "right",
    "esc",
    "enter",
    "turn_off"
};

bool Event::WriteEventName(std::ostream& stream) const {
    if (source >= InputSources::input_source_none && 
        source < InputSources::input_source_count &&
        key_action >= KeyAction::key_action_none && 
        key_action < KeyAction::key_action_count &&
        key_code >= KeyCode::key_code_none && 
        key_code < KeyCode::key_code_count) 
    {
        stream << "from " << input_sources_names[static_cast<std::uint32_t>(source)] << " ";
        if (source == InputSources::input_source_controller) {
            stream << "#" << joystick_number << " ";
        }
        

        stream << "key " << key_code_names[static_cast<std::uint32_t>(key_code)] << " "
               << key_action_names[static_cast<std::uint32_t>(key_action)];
        return stream.good();
    } else {   
        return false;
    }   
}

bool Event::CanContinue() const {
    return key_code != KeyCode::key_code_turn_off;
}

Event::~Event() {

}

std::ostream& operator<<(std::ostream& stream, const Event& event) {
    if (!event.WriteEventName(stream)) {
        throw std::runtime_error("too big event value");
    }
    return stream;
}

Engine::~Engine() {}

#pragma pack(push, 4)
template <typename T>
struct key_bind {
    T       key;
    KeyCode key_code;
};
#pragma pack(pop)


const std::array<key_bind<SDL_Keycode>, 10> keypad_keys { 
    { 
        { SDLK_w,      KeyCode::key_code_up},
        { SDLK_a,      KeyCode::key_code_left},
        { SDLK_s,      KeyCode::key_code_down},
        { SDLK_d,      KeyCode::key_code_right},
        { SDLK_UP,     KeyCode::key_code_up},
        { SDLK_LEFT,   KeyCode::key_code_left},
        { SDLK_DOWN,   KeyCode::key_code_down},
        { SDLK_RIGHT,  KeyCode::key_code_right},
        { SDLK_ESCAPE, KeyCode::key_code_esc},
        { SDLK_RETURN, KeyCode::key_code_enter} 
    } 
};

const std::array<key_bind<SDL_GameControllerButton>, 6> controller_keys { 
    { 
        { SDL_CONTROLLER_BUTTON_Y,     KeyCode::key_code_up},  
        { SDL_CONTROLLER_BUTTON_X,     KeyCode::key_code_left},
        { SDL_CONTROLLER_BUTTON_A,     KeyCode::key_code_down},
        { SDL_CONTROLLER_BUTTON_B,     KeyCode::key_code_right},
        { SDL_CONTROLLER_BUTTON_BACK,  KeyCode::key_code_esc}, 
        { SDL_CONTROLLER_BUTTON_START, KeyCode::key_code_enter}
    } 
};

static bool check_keypad_key_code(const SDL_Event& event, KeyCode& result) {
    const auto it = std::find_if(std::begin(keypad_keys), 
                                 std::end(keypad_keys), 
                                 [&](const key_bind<SDL_Keycode>& binds) {
        return binds.key == event.key.keysym.sym;
        });

    if (it != end(keypad_keys)) {
        result = it->key_code;
        return true;
    }
    return false;
}

static bool check_controller_key_code(const SDL_Event& event, KeyCode& result, int& joystick_number) {
    const auto it = std::find_if(std::begin(controller_keys), 
                                 std::end(controller_keys), 
                                 [&](const key_bind<SDL_GameControllerButton>& binds) {
        return binds.key == event.cbutton.button;
        });
    
    if (it != end(controller_keys)) {
        result =  it->key_code;
        joystick_number = event.cdevice.which;
        return true;
    }
    return false;
}

// only here will be work with sdl
class EngineImpl final : public Engine {
public:
    EngineImpl() 
        : Engine()
        , has_controller(false)
        , window(nullptr)
    {
    }

    std::string Initialize(std::string_view /*config*/) {
        std::stringstream serr;

        SDL_version compiled = { 0, 0, 0 };
        SDL_version linked   = { 0, 0, 0 };

        SDL_VERSION(&compiled);
        SDL_GetVersion(&linked);

        if (SDL_COMPILEDVERSION !=
            SDL_VERSIONNUM(linked.major, linked.minor, linked.patch)) {   
            serr << "warning: SDL2 compiled and linked version mismatch: "
                 << compiled << " " << linked << std::endl;
        }

        const int init_result = SDL_Init(SDL_INIT_EVERYTHING);
        if (init_result != 0) {
            const char* err_message = SDL_GetError();
            serr << "error: failed call SDL_Init: " << err_message << std::endl;
            return serr.str();
        }

        const SDL_Window* window = SDL_CreateWindow("test",
            SDL_WINDOWPOS_CENTERED,
            SDL_WINDOWPOS_CENTERED,
            640,
            480,
            ::SDL_WINDOW_OPENGL);

        if (window == nullptr) {
            const char* err_message = SDL_GetError();
            serr  << "error: failed call SDL_CreateWindow: " << err_message << std::endl;
            SDL_Quit();
            return serr.str();
        }

        const auto joysticks_count = SDL_NumJoysticks();
        has_controller = joysticks_count > 0;
        if (has_controller) {
            serr << "Count joysticks connected: " << joysticks_count << std::endl;
        } else {
            serr << "warning: no joysticks connected" << std::endl;
        }
        return "";
    }

    bool ReadInput(Event& e) final {
        SDL_Event sdl_event;
        if (SDL_PollEvent(&sdl_event)) {
            KeyCode current_key_code;
            if (sdl_event.type == SDL_QUIT) {
                e = Event(InputSources::input_source_none, KeyAction::key_action_none, KeyCode::key_code_turn_off);
                return true;
            } else if (sdl_event.type == SDL_KEYDOWN ||
                       sdl_event.type == SDL_KEYUP) {
                if (check_keypad_key_code(sdl_event, current_key_code)) {
                    if (sdl_event.type == SDL_KEYDOWN) {
                        e = Event(InputSources::input_source_keypad, KeyAction::key_action_pressed, current_key_code);
                    } else if (sdl_event.type == SDL_KEYUP) {
                        e = Event(InputSources::input_source_keypad, KeyAction::key_action_released, current_key_code);
                    }
                    return true;
                }
            } else if (has_controller && (sdl_event.type == SDL_CONTROLLERBUTTONDOWN ||
                                          sdl_event.type == SDL_CONTROLLERBUTTONUP)) {
                int joy_num;
                if (check_controller_key_code(sdl_event, current_key_code, joy_num)) {
                    if (sdl_event.type == SDL_CONTROLLERBUTTONDOWN) {
                        e = Event(InputSources::input_source_controller, KeyAction::key_action_pressed, current_key_code, joy_num);
                    } else if (sdl_event.type == SDL_CONTROLLERBUTTONUP) {
                        e = Event(InputSources::input_source_controller, KeyAction::key_action_released, current_key_code, joy_num);
                    }
                    return true;
                }
            }
        }    
        return false;
    }

    void Uninitialize() final {
        if (window != nullptr) {
            SDL_DestroyWindow(window);
        }
        SDL_Quit();
    }

private:
    bool has_controller;
    SDL_Window* window;
};

Engine* create_engine() {
    if (engine_already_exist) {
        throw std::runtime_error("engine already exist");
    }
    Engine* result = new EngineImpl();
    engine_already_exist  = true;
    return result;
}

void destroy_engine(Engine* eng) {
    if (!engine_already_exist) {
        throw std::runtime_error("engine not created");
    }
    if (eng == nullptr) {
        throw std::runtime_error("current engine is nullptr");
    }
    engine_already_exist = false;
    delete eng;
    eng = nullptr;
}

} // end namespace rh 
