#include <algorithm>
#include <array>
#include <cstdlib>
#include <iostream>
#include <string_view>

#if __has_include(<SDL.h>)
#include <SDL.h>
#else
#include <SDL2/SDL.h>
#endif

std::ostream& operator<<(std::ostream& out, const SDL_version& v) {
    out << static_cast<int>(v.major) << '.';
    out << static_cast<int>(v.minor) << '.';
    out << static_cast<int>(v.patch);
    return out;
}


#pragma pack(push, 4)
template <typename T>
struct key_bind {
    T                key;
    std::string_view name;
};
#pragma pack(pop)

const std::array<::key_bind<SDL_Keycode>, 10> keypad_keys { 
    { 
        { SDLK_w,      "up (w)" },
        { SDLK_a,      "left (a)" },
        { SDLK_s,      "down (s)" },
        { SDLK_d,      "right (d)" },
        { SDLK_UP,     "up" },
        { SDLK_LEFT,   "left" },
        { SDLK_DOWN,   "down" },
        { SDLK_RIGHT,  "right" },
        { SDLK_ESCAPE, "esc" },
        { SDLK_RETURN, "enter" } 
    } 
};

const std::array<::key_bind<SDL_GameControllerButton>, 6> controller_keys { 
    { 
        { SDL_CONTROLLER_BUTTON_Y,     "up (y)" },
        { SDL_CONTROLLER_BUTTON_X,     "left (x)" },
        { SDL_CONTROLLER_BUTTON_A,     "down (a)" },
        { SDL_CONTROLLER_BUTTON_B,     "right (b)" },
        { SDL_CONTROLLER_BUTTON_BACK,  "back" },
        { SDL_CONTROLLER_BUTTON_START, "start" } 
    } 
};

void check_keypad_key(const SDL_Event& event) {
    const auto it = std::find_if(std::begin(keypad_keys), 
                                 std::end(keypad_keys), 
                                 [&](const ::key_bind<SDL_Keycode>& binds) {
        return binds.key == event.key.keysym.sym;
        });

    if (it != end(keypad_keys)) {
        std::cout << it->name << ' ';
        if (event.type == SDL_KEYDOWN) {
            std::cout << "key press detected" << std::endl;
        } else if (event.type == SDL_KEYUP) {
            std::cout << "key release detected" << std::endl;
        }
    }
}

void check_controller_key(const SDL_Event& event) {
    const auto it = std::find_if(std::begin(controller_keys), 
                                 std::end(controller_keys), 
                                 [&](const ::key_bind<SDL_GameControllerButton>& binds) {
        return binds.key == event.cbutton.button;
        });

    if (it != end(controller_keys)) {
        std::cout << it->name << ' ';
        if (event.type == SDL_CONTROLLERBUTTONDOWN) {
            std::cout << "key press detected";
        } else if (event.type == SDL_CONTROLLERBUTTONUP) {
            std::cout << "key release detected";
        }
        std::cout << " from controller #" << event.cdevice.which << std::endl;
    }
}

bool check_input(const SDL_Event& event, const bool has_controller) {
    bool continue_code = true;

    if (event.type == SDL_KEYDOWN || 
        event.type == SDL_KEYUP) {
        check_keypad_key(event);
    } else if (has_controller && (event.type == SDL_CONTROLLERBUTTONDOWN || 
                                  event.type == SDL_CONTROLLERBUTTONUP)) {
        check_controller_key(event);
    } else if (event.type == SDL_QUIT) {
        continue_code = false;
    }
    
    return continue_code;
}

int main(int /*argc*/, char* /*argv*/[]) {
    SDL_version compiled = { 0, 0, 0 };
    SDL_version linked   = { 0, 0, 0 };

    SDL_VERSION(&compiled);
    SDL_GetVersion(&linked);

    if (SDL_COMPILEDVERSION !=
        SDL_VERSIONNUM(linked.major, linked.minor, linked.patch)) {   
        std::cerr << "warning: SDL2 compiled and linked version mismatch: "
                  << compiled << " " << linked << std::endl;
    }

    const int init_result = SDL_Init(SDL_INIT_EVERYTHING);
    if (init_result != 0) {
        const char* err_message = SDL_GetError();
        std::cerr << "error: failed call SDL_Init: " << err_message << std::endl;
        return EXIT_FAILURE;
    }

    SDL_Window* const window = SDL_CreateWindow("test",
                                                SDL_WINDOWPOS_CENTERED,
                                                SDL_WINDOWPOS_CENTERED,
                                                640,
                                                480,
                                                ::SDL_WINDOW_OPENGL);

    if (window == nullptr) {
        const char* err_message = SDL_GetError();
        std::cerr << "error: failed call SDL_CreateWindow: " << err_message << std::endl;
        SDL_Quit();
        return EXIT_FAILURE;
    }
    
    const auto joysticks_count = SDL_NumJoysticks();
    const bool has_controller = joysticks_count > 0;
    if (has_controller) {
        std::cerr << "Count joysticks connected: " << joysticks_count << std::endl;
    } else {
        std::cerr << "warning: no joysticks connected" << std::endl;
    }

    bool continue_loop = true;
    while (continue_loop) {
        SDL_Event sdl_event;

        while (SDL_PollEvent(&sdl_event)) {
            continue_loop = check_input(sdl_event, has_controller);
        }
    }

    SDL_DestroyWindow(window);
    SDL_Quit();

    return EXIT_SUCCESS;
}
