#include <gtest/gtest.h>

#include <hello_user.h>

TEST(hello_user, PositiveNos) {
    testing::internal::CaptureStdout();
    int result_code = hello_user("test");
    std::string result_cout_string = testing::internal::GetCapturedStdout();
    ASSERT_EQ(EXIT_SUCCESS, result_code);
    ASSERT_EQ("Hello, test\n", result_cout_string);
}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}

