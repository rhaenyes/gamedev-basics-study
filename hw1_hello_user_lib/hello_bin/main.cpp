#include <cstdlib>

#include <hello_user.h>

int main(int /*argc*/, char** /*argv*/) {
    const char* user_env_var = std::getenv("USER");
    std::string_view user = user_env_var != nullptr
                          ? user_env_var
                          : "UNKNOWN_USER (USER environment variable not found)";

    int result_code = hello_user(user);
    return result_code;
}
