cmake_minimum_required(VERSION 3.16)

project(hello_user)

add_subdirectory(hello_lib)
add_subdirectory(hello_bin)

