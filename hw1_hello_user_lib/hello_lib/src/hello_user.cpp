// #include "hello_user.h"

#include <iostream>

int hello_user(const std::string_view& user_name) {
    std::cout << "Hello, " << user_name << std::endl;
    return std::cout.good() ? EXIT_SUCCESS : EXIT_FAILURE;
}

