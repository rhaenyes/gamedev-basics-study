#include "hello_world.h"

#include <iostream>

int print_hello_world() {
    std::cout << "Hello, world" << std::endl;
    return std::cout.good() ? EXIT_SUCCESS : EXIT_FAILURE;
}

