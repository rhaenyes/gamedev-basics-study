#include "hello_world.h"

#include <gtest/gtest.h>

TEST(print_hello_world, PositiveNos) {
    testing::internal::CaptureStdout();
    int result_code = print_hello_world();
    std::string result_cout_string = testing::internal::GetCapturedStdout();
    ASSERT_EQ(EXIT_SUCCESS, result_code);
    ASSERT_EQ("Hello, world\n", result_cout_string);
}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}
